import Toybox.Application.Storage;
import Toybox.Application;

(:background)
class DevicesManager {
  private var mDevices as Array;

  function initialize() {
    mDevices = [];
    // cleanDeviceStorage();
    loadDevicesFromStorage();
  }

  function onPhoneAppMessage(messageArr) {
    if (messageArr[0].equals("sync")) {
      storeDeviceMessage(messageArr);
      loadDevicesFromStorage();
      sendGarminStatus();
    }
  }

  function onBackgroundData(messageArr as Array) {
    System.println("onBackgroundData");
    System.println(messageArr);
    
    if (messageArr[0].equals("sync")) {
      loadDevicesFromStorage();
    }
    // interpret requestpairing responses
    if (messageArr[0].equals("requestpairing")) {
      processPairDeviceResponse(messageArr);
    }
  }

  function pairDevice(device as Device) {
    Storage.setValue("lastPairingDevice", device.getId());
    var message = new Message();
    message.setCommand("requestpairing");
    message.setArgs([device.getId()]);
    message.send();
  }

  function processPairDeviceResponse(messageArr as Array) {
    var deviceId = messageArr[1];
    var allow = messageArr[2].equals("allow") ? true : false;
    if (allow) {  
      // find device and set paired to true
      for (var i = 0; i < mDevices.size(); i++) {
        if (mDevices[i].getId().equals(deviceId)) {
          mDevices[i].setStatus(1);
        }
      }
      storeDevices();
      Application.getApp().processPairDeviceSuccess(messageArr);
    }
    //close the pairingprogress
  }

  function getDevices() {
    return mDevices;
  }

  function getLastPairingDevice() as Device {
    var lastPairingDevice = Storage.getValue("lastPairingDevice");
    if (lastPairingDevice) {
      return getDeviceById(lastPairingDevice);
    }
    return null;
  }

  function getDeviceById(id as String) as Device {
    var result = null;
    for (var i = 0; i < mDevices.size(); i++) {
      if (mDevices[i].getId().equals(id)) {
        result = mDevices[i];
      }
    }
    return result;
  }

  private function storeDevices() {
    var devices = [];
    for (var i = 0; i < mDevices.size(); i++) {
      devices.addAll(mDevices[i].serialize());
    }
    Storage.setValue("devices", devices);
  }

  private function storeDeviceMessage(deviceMessage as Array) {
    var devices = [];
    for (var i = 1; i < deviceMessage.size(); i++) {
      devices.add(deviceMessage[i]);
    }
    Storage.setValue("devices", devices);
  }

  private function cleanDeviceStorage() {
    Storage.setValue("devices", []);
  }

  private function loadDevicesFromStorage() {
    // load and parse devices from Storage to mDevices
    var deviceMessage = Storage.getValue("devices");
    if (deviceMessage == null) {
      return;
    }
    if (deviceMessage.size() > 0) {
      mDevices = [];
      var deviceMessageLength = 5;
      var devicesPage = 0;
      var currentDataSkip = devicesPage * deviceMessageLength;

        for (
          var i = 0;
          i < deviceMessage.size();
          i = devicesPage * deviceMessageLength
        ) {
          var device = new Device(
            deviceMessage[i],
            deviceMessage[i + 1],
            deviceMessage[i + 2],
            deviceMessage[i + 3]
          );
          // TODO: optimize following function
          if (deviceMessage[i + 4] instanceof Lang.Array) {
            device.setCapabilities(deviceMessage[i + 4]);
          } else {
            device.parseCapabilities(deviceMessage[i + 4]);
          }
          mDevices.add(device);

          devicesPage++;
        }
    }
  }

  private function sendGarminStatus() as Void {
    var systemStats = System.getSystemStats();
    var status = [systemStats.battery, systemStats.charging];
    var commListener = new CommListener();
    var message = new Message();
    message.setCommand("sync");
    message.setArgs([status[0].toString(), status[1].toString()]);
    message.send();
  }
}
