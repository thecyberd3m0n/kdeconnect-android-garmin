import Toybox.Communications;

(:background)
class PluginMessage {
    private var mCommand as String;
    private var mArgs as Array;
    private var mPlugin as String;
    public function initialize() {
    }

    function setPlugin(pluginName as String) {
        mPlugin = pluginName;
    }

    function getPlugin() {
        return mPlugin;
    }

    function setCommand(command as String) {
        mCommand = command;
    }

    function setArgs(args as Array) {
        mArgs = args;
    }

    function getCommand() {
        return mCommand;
    }

    function getArgs() {
        return mArgs;
    }

    function send() {
        var messageArr = ["plugin", getPlugin(), getCommand()];
        if (getArgs() == null) {
            System.println("Cannot send message");
            return;
        }
        for (var i = 0; i < getArgs().size(); i++) {
            var arg = getArgs()[i];
            messageArr.add(arg);
        }

        var connectionListener = new CommListener();
        try {
            Communications.transmit(messageArr, null, connectionListener);
        } catch (ex) {
            System.println(ex.getErrorMessage());
        }
    }

    static function fromArray(message as Array) {
        var nm = new Message();
        nm.setCommand(message[0]);
        nm.remove(0);
        nm.setArgs(message);
    }
}
