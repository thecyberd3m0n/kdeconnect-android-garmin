class PluginBackgroundDataListener {
    private var mPlugin as String;
    private var mCommand as String;
    private var mCallback as Method;
    function initialize(
        plugin as String,
        command as String,
        callback as Method
    ) {
        mPlugin = plugin;
        mCommand = command;
        mCallback = callback;
    }

    function getPlugin() as String {
        return mPlugin;
    }

    function getCommand() as String {
        return mCommand;
    }

    function getCallback() as Method {
        return mCallback;
    }
}