using Toybox.WatchUi;

class SyncProgress extends WatchUi.ProgressBar {
    function initialize(title as String) {
        ProgressBar.initialize(title, null);
    }

    // Resources are loaded here
    function onLayout(dc) {
        setLayout(Rez.Layouts.MainLayout(dc));
    }

    // onShow() is called when this View is brought to the foreground
    function onShow() {
    }

    // onUpdate() is called periodically to update the View
    function onUpdate(dc) {
        View.onUpdate(dc);
        System.println("progress on update");
    }

    // onHide() is called when this View is removed from the screen
    function onHide() {
    }
}