import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;
import Toybox.Communications;
import Toybox.System;
import Toybox.Time;
import Toybox.Timer;

(:background)
var globalMember;

// TODO: dedupe
(:background)
class CommListener extends Communications.ConnectionListener {
  function initialize() {
    Communications.ConnectionListener.initialize();
  }
  function onComplete() {
    System.println("Transmit Complete");
  }

  function onError() {
    System.println("Transmit Failed");
  }
}


(:background)
class KDEConnectIQApp extends Application.AppBase {
  private var mProgressBar = null;
  private var mDevicesManager as DevicesManager;
  private var backgroundDataListeners = [];
  function initialize() {
    AppBase.initialize();
    if (!(Background.getPhoneAppMessageEventRegistered())) {
      Background.registerForPhoneAppMessageEvent();
    } else {
      Background.deletePhoneAppMessageEvent();
      Background.registerForPhoneAppMessageEvent();
    }

    // Register to run every five minutes
    $.globalMember = true;
  }

  // onStart() is called on application start up
  function onStart(state as Dictionary?) as Void {

  }

  public function getServiceDelegate() as ServiceDelegate {
    return [new SyncServiceDelegate()];
  }

  // onStop() is called when your application is exiting
  function onStop(state as Dictionary?) as Void {}

  function onBackgroundData(data) {
    if (data[0].equals("plugin")) {
      for (var i = 0; i < backgroundDataListeners.size(); i++) {
        if (backgroundDataListeners[i].getPlugin().equals(data[1]) || backgroundDataListeners[i].getCommand().equals(data[2])) {
          backgroundDataListeners[i].getCallback().invoke(data);
        }
      }
    } else {
      mDevicesManager = new DevicesManager();
      mDevicesManager.onBackgroundData(data);
    }

    WatchUi.requestUpdate();
  }

  function addBackgroundDataListener(pluginBackgroundDataListener as PluginBackgroundDataListener) {
    backgroundDataListeners.add(pluginBackgroundDataListener);
  }

  function removeBackgroundDataListener(plugin as String, command as String) {
    for (var i = 0; i < backgroundDataListeners.size(); i++) {
      if (backgroundDataListeners[i].getPlugin().equals(plugin) || backgroundDataListeners[i].getCommand().equals(command)) {
        backgroundDataListeners.remove(backgroundDataListeners[i]);
      }
    }
  }

  function pairDevice(device as Device) {
    mDevicesManager = new DevicesManager();
    mDevicesManager.pairDevice(device);
    mProgressBar = new WatchUi.ProgressBar("Pairing " + device.getName(), null);
    WatchUi.pushView(mProgressBar, null, WatchUi.SLIDE_IMMEDIATE);
  }

  function processPairDeviceSuccess(data) {
    if (data[2].equals("allow") && mProgressBar != null) {
      mProgressBar.setDisplayString("Paired");
      mProgressBar.setProgress(100);
      var mTimer = new Timer.Timer();
      var lastPairingDevice = Storage.getValue("lastPairingDevice");
      if (lastPairingDevice != null) {
        mTimer.start(method(:goToDevice), 2000, false);
      }
      
      // TODO: display success message
    }
    // TODO: display dialog that is successful
    WatchUi.requestUpdate();

  }

  function goToDevice() as Void {
      var devicesManager = new DevicesManager();
      var device = devicesManager.getLastPairingDevice();
      var menu = new PairedDeviceMenu(device);
      var pairedDeviceMenuDelegate = new PairedDeviceMenuDelegate(device);
      WatchUi.pushView(menu, pairedDeviceMenuDelegate, WatchUi.SLIDE_IMMEDIATE);
  }

  // Return the initial view of your application here
  function getInitialView() as Array<Views or InputDelegates>? {
    var devicesView = new DevicesView(0);
    return [devicesView, new DeviceViewInputDelegate(devicesView)] as Array<Views or InputDelegates>;
  }
}

function getApp() as KDEConnectIQApp {
  return Application.getApp() as KDEConnectIQApp;
}
