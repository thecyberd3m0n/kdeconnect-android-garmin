import Toybox.Communications;

(:background)
class Message {
    private var mCommand as String;
    private var mArgs as Array;

    public function initialize() {
    }

    function setCommand(command as String) {
        mCommand = command;
    }

    function setArgs(args as Array) {
        mArgs = args;
    }

    function getCommand() {
        return mCommand;
    }

    function getArgs() {
        return mArgs;
    }

    function send() {
        var messageArr = [getCommand()];
        if (getArgs() == null) {
            System.println("Cannot send message");
            return;
        }
        for (var i = 0; i < getArgs().size(); i++) {
            var arg = getArgs()[i];
            messageArr.add(arg);
        }
        var connectionListener = new CommListener();
        try {
            Communications.transmit(messageArr, null, connectionListener);
        } catch (ex) {
            System.println(ex.getErrorMessage());
        }
    }

    static function fromArray(message as Array) {
        var nm = new Message();
        nm.setCommand(message[0]);
        nm.remove(0);
        nm.setArgs(message);
    }
}
