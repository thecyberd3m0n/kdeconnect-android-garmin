import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;

class UnpairedDeviceMenuDelegate extends WatchUi.MenuInputDelegate {
    private var mRelatedView;
    function initialize(relatedView as DevicesView) {
        MenuInputDelegate.initialize();
        mRelatedView = relatedView;
    }

    function onMenuItem(item as Symbol) as Void {
        if (item == :pair) {
            // show pairing progressbar
            mRelatedView.pairDevice();
        }
        if (item == :cancel) {
            WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
        }
    }

}