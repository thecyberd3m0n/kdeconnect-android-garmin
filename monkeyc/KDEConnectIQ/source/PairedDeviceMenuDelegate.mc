import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;

class PairedDeviceMenuDelegate extends WatchUi.MenuInputDelegate {
    private var mDevice;
    function initialize(device as Device) {
        MenuInputDelegate.initialize();
        mDevice = device;
    }

    function onMenuItem(item as Symbol) as Void {
        switch (item) {
            case "RunCommandPlugin":
                var view = new RunCommandsView(mDevice);
                var delegate = new RunCommandsViewDelegate(mDevice);
                WatchUi.pushView(view, delegate, WatchUi.SLIDE_IMMEDIATE);
                break;
        }   
    }
}
