import Toybox.WatchUi;

class PairedDeviceMenu extends WatchUi.Menu {
    private var mDevice;
    private var mCapabilities = [];
    static var LABELS = {
        "RunCommandPlugin" => "Run Script" 
    };
    function initialize(device as Device) {
        WatchUi.Menu.initialize();
        mDevice = device;
        mCapabilities = mDevice.getCapabilities();

        setTitle(mDevice.getName());
        for (var i = 0; i < mCapabilities.size(); i++) {
            if (PairedDeviceMenu.LABELS.hasKey(mCapabilities[i])) {
                addItem(PairedDeviceMenu.LABELS.get(mCapabilities[i]), mCapabilities[i]);
            }
        }
    }

}
