import Toybox.WatchUi;
import Toybox.Application;
import Toybox.Timer;


class RunCommandsMenuDelegate extends WatchUi.MenuInputDelegate {
    private var mCommands = [];
    private var mDevice as Device;
    private var mProgressBar as ProgressBar;
    function initialize(device as Device, commands as Array) {
        MenuInputDelegate.initialize();
        mCommands = commands;
        mDevice = device;
    }

    function onMenuItem(item as Symbol) as Void {
        if (item.equals("addscript")) {
            sendAddScriptCommand();
        } else {
            sendCommand(item);
        }
    }

    private function sendCommand(commandId as String) as Void {
        var message = new PluginMessage();
        message.setPlugin("RunCommandPlugin");
        message.setCommand("send");
        message.setArgs([mDevice.getId(), commandId]);
        message.send();
        mProgressBar = new WatchUi.ProgressBar("Sending command", null);
        WatchUi.pushView(mProgressBar, null, SLIDE_IMMEDIATE);
        var pluginBackgroundDataListener = new PluginBackgroundDataListener("RunCommandPlugin", "sent", method(:onCommandSent));
        Application.getApp()
            .addBackgroundDataListener(pluginBackgroundDataListener);
    }

    private function sendAddScriptCommand() as Void {
        var message = new PluginMessage();
        message.setPlugin("RunCommandPlugin");
        message.setCommand("addscript");
        message.setArgs([mDevice.getId()]);
        message.send();
        mProgressBar = new WatchUi.ProgressBar("Sending AddScript command", null);
        WatchUi.pushView(mProgressBar, null, SLIDE_IMMEDIATE);
        var pluginBackgroundDataListener = new PluginBackgroundDataListener("RunCommandPlugin", "addscript", method(:onAddScriptCommandSent));
        Application.getApp()
            .addBackgroundDataListener(pluginBackgroundDataListener);
    }

    function onAddScriptCommandSent(data) {
        mProgressBar.setDisplayString("Add new script on computer screen");
        mProgressBar.setProgress(100);
        var mTimer = new Timer.Timer();
        mTimer.start(method(:removeProgressBar), 2000, false);
        Application.getApp().removeBackgroundDataListener("RunCommandPlugin", "addscript");
    }

    function onCommandSent(data) {
        mProgressBar.setDisplayString("Command sent");
        mProgressBar.setProgress(100);
        var mTimer = new Timer.Timer();
        mTimer.start(method(:removeProgressBar), 2000, false);
        Application.getApp().removeBackgroundDataListener("RunCommandPlugin", "sent");
    }

    function removeProgressBar() {
        WatchUi.popView(SLIDE_IMMEDIATE);
    }

}
