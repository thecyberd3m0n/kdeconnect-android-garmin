import Toybox.WatchUi;
import Toybox.Application;

class RunCommandsView extends WatchUi.View {
    private var mDevice;
    private var mProgressBar = null;
    private var commands = [];
    private var canAddScripts = false;

    function initialize(device as Device) {
        View.initialize();
        mDevice = device;
    }

    function onLayout(dc) {
        mProgressBar = new WatchUi.ProgressBar("Loading scripts", null);
        WatchUi.pushView(mProgressBar, null, WatchUi.SLIDE_IMMEDIATE);

    }

    function onShow() as Void {
        // load scripts for device
        if (commands.size() == 0) {
            var pluginBackgroundDataListener = new PluginBackgroundDataListener("RunCommandPlugin", "load", method(:onDataLoaded));
            Application.getApp().addBackgroundDataListener(pluginBackgroundDataListener);
            var message = new Message();
            message.setCommand("plugin");
            message.setArgs(["RunCommandPlugin", "load", mDevice.getId()]);
            message.send();
        } else {
            WatchUi.popView(SLIDE_UP);
        }
    }

    function renderCommandsMenu() {
        var menu = new WatchUi.Menu();
        var delegate = new RunCommandsMenuDelegate(mDevice, commands);
        if (commands.size() > 0) {
            menu.setTitle("Scripts for " + mDevice.getName());
            for (var i = 0; i < commands.size(); i++) {
                menu.addItem(commands[i].getName(), commands[i].getId());
            }
        }
        if (canAddScripts == true) {
            menu.addItem("Add script", "addscript");
        }
        WatchUi.switchToView(menu, delegate, WatchUi.SLIDE_IMMEDIATE);
    }

    function onDataLoaded(data) {
        if (mDevice.getId().equals(data[3])) {
            Application.getApp().removeBackgroundDataListener("RunCommandPlugin", "load");
            canAddScripts = data[4].equals("true") ? true : false;
            commands = parseCommands(data);
            renderCommandsMenu();            
        }
    }

    private function parseCommands(data as Array) {
        var commands = [];
        var commandsStart = 5;
        var commandsPage = 0;
        var commandsMessageLength = 3;
        for (var i = commandsStart; i < data.size(); i = commandsStart + (commandsPage * commandsMessageLength)) {
            var command = new Command(data[i], data[i + 1], data[i + 2]);
            commands.add(command);
            commandsPage++;
        }
        return commands;
    }
}
