import Toybox.System;

(:background)
class SyncServiceDelegate extends System.ServiceDelegate {
  function initialize() {
    ServiceDelegate.initialize();
  }

  public function onPhoneAppMessage(messageArr) as Void {
    System.println("receive message");
    System.println(messageArr.data);
    var deviceManager = new DevicesManager();
    deviceManager.onPhoneAppMessage(messageArr.data);

    Background.exit(messageArr.data);
  }

}
