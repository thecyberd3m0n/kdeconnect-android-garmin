import Toybox.System;
import Toybox.WatchUi;


class DeviceViewInputDelegate extends WatchUi.InputDelegate {
    private var mRelatedView;
  //! Constructor
  //! @param relatedView The view this delegate is tied to
  //! @param callback The method of the calling view to return a result to
  function initialize(relatedView) {
    InputDelegate.initialize();
    mRelatedView = relatedView;
  }

  function onKey(keyEvent as WatchUi.KeyEvent) {
    if (keyEvent.getKey() == WatchUi.KEY_ENTER) {
      mRelatedView.openDeviceOptions();
    }
  }

  //! Invoke the callback method and pop the dialog view.
  function onSwipe(swipeEvent as SwipeEvent) {
    System.println(swipeEvent.getDirection());
    if (swipeEvent.getDirection() == WatchUi.SWIPE_LEFT) {
      mRelatedView.nextPage();
    }
    if (swipeEvent.getDirection() == WatchUi.SWIPE_RIGHT) {
      mRelatedView.prevPage();
    }
    if (swipeEvent.getDirection() == WatchUi.SWIPE_UP) {
      mRelatedView.openDeviceOptions();
    }
    return true;
  }
}