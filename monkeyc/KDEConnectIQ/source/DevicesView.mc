import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.System;
import Toybox.Application;
class DevicesView extends WatchUi.View { 
  private var deviceMessageLength = 4;
  private var mDevices;
  private var mCurrentDeviceIndex = 0;
  private var mCurrentDevice = []; // TODO: type
  private var pairingProgressBar = null;

  function initialize(currentIndex) {
    mCurrentDeviceIndex = currentIndex;
    View.initialize();
  }

  // Load your resources here
  function onLayout(dc as Dc) as Void {
    setLayout(Rez.Layouts.MainLayout(dc));
  }

  // Called when this View is brought to the foreground. Restore
  // the state of this View and prepare it to be shown. This includes
  // loading resources into memory.
  function onShow() as Void {
    render();
  }

  // Update the view
  function onUpdate(dc as Dc) as Void {
    render();
    View.onUpdate(dc);
  }
  
  function render() {
    var devicesManager = new DevicesManager();
    mDevices = devicesManager.getDevices();

    if (mDevices == null || mDevices.size() == 0) {
        renderNoDevices();

        return;
    }
    mCurrentDevice = mDevices[mCurrentDeviceIndex];
    
    try {
      View.findDrawableById("no_devices").setVisible(false);
      View.findDrawableById("device_status").setVisible(true);
      View.findDrawableById("device_name_label").setVisible(true);
      View.findDrawableById("device_name_label").setText(mCurrentDevice.getName());
      if (mCurrentDevice.getStatus() == 2) {
        View.findDrawableById("device_status").setText(
          WatchUi.loadResource(Rez.Strings.device_status_not_paired)
        );
      } else {
        View.findDrawableById("device_status").setText(
          WatchUi.loadResource(Rez.Strings.device_status_paired)
        );
      }
      if (mCurrentDevice.getDeviceType() == 0) {
        View.findDrawableById("DevicePc").setVisible(true);
      }
      if (mCurrentDevice.getDeviceType() == 1) {
        View.findDrawableById("DevicePhone").setVisible(true);
      }
      if (mCurrentDevice.getDeviceType() == 2) {
        View.findDrawableById("DevicePhone").setVisible(true);
      }
    } catch (ex) {
      System.println(ex.getErrorMessage());
    }
  }

  function renderNoDevices() {
    View.findDrawableById("DevicePc").setVisible(false);
    View.findDrawableById("device_status").setVisible(false);
    View.findDrawableById("device_name_label").setVisible(false);
    View.findDrawableById("no_devices").setVisible(true);
  }

  // Called when this View is removed from the screen. Save the
  // state of this View here. This includes freeing resources from
  // memory.
  function onHide() as Void {}

  function nextPage() {
    if (mCurrentDeviceIndex + 1 < mDevices.size()) {
      var devicesView = new DevicesView(mCurrentDeviceIndex + 1);
      WatchUi.pushView(
        devicesView,
        new DeviceViewInputDelegate(devicesView),
        WatchUi.SLIDE_LEFT
      );
    }
  }

  function prevPage() {
    WatchUi.popView(WatchUi.SLIDE_LEFT);
  }

  function openDeviceOptions() as Void {
    if (mCurrentDevice.getStatus() == 2) {
      // open NotPairedMenu
      var menu = new WatchUi.Menu();
      var delegate = new UnpairedDeviceMenuDelegate(self);

      var pairItem = menu.addItem(WatchUi.loadResource(Rez.Strings.pair), :pair);

      menu.addItem(WatchUi.loadResource(Rez.Strings.cancel), :cancel);
      menu.setTitle(mCurrentDevice.getName());
      WatchUi.pushView(menu, delegate, WatchUi.SLIDE_UP);
    } else {
      var menu = new PairedDeviceMenu(mCurrentDevice);
      var delegate = new PairedDeviceMenuDelegate(mCurrentDevice);
      WatchUi.pushView(menu, delegate, WatchUi.SLIDE_UP);
    }
  }

  function pairDevice() as Void {
    Application.getApp().pairDevice(mCurrentDevice);
  }
}
