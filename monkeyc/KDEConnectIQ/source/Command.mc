class Command {
    private var mId as String;
    private var mName as String;
    private var mCommand as String;
    function initialize(
        commandId as String, 
        commandName as String, 
        command as String) {
            mId = commandId;
            mName = commandName;
            mCommand = command;
        }

    function getId() as String {
        return mId;
    }

    function getName() as String {
        return mName;
    }

    function getCommand() as String {
        return mCommand;
    }
}
