import Toybox.System;

(:background)
class Device {
  private var mDeviceId as String;
  private var mDeviceName as String;
  private var mDeviceType as DeviceType;
  private var mDeviceStatus as DeviceStatus;
  private var mCapabilities as Array = [];
  private static var mSupportedCapabilities = [
    "RunCommandPlugin",
    "BatteryPlugin",
  ];

  function initialize(
    deviceId as String,
    deviceName as String,
    deviceType as String,
    deviceStatus as String
  ) {
    mDeviceId = deviceId;
    mDeviceName = deviceName;
    mDeviceType = deviceType.equals("desktop") ? 0 : 1;
    mDeviceStatus = deviceStatus.equals("true") ? 1 : 2;
  }

  function getId() as String {
    return mDeviceId;
  }

  function getName() as String {
    return mDeviceName;
  }

  function getType() {
    return mDeviceType;
  }

  function getStatus() as DeviceStatus {
    return mDeviceStatus;
  }

  function setStatus(deviceStatus as DeviceStatus) {
    mDeviceStatus = deviceStatus;
  }

  function getDeviceType() as String {
    return mDeviceType;
  }

  function getCapabilities() as Array {
    return mCapabilities;
  }

  function serialize() as Array {
    return [
      getId(),
      getName(),
      getType() == 0 ? "desktop" : "mobile",
      getStatus() == 2 ? "false" : "true",
      getCapabilities(),
    ];
  }

  function setCapabilities(capabilities as Array) {
    mCapabilities = capabilities;
  }

  function parseCapabilities(capabilitiesString as String) {
    var capabilitiesChars = capabilitiesString.toCharArray();
    var word = [];
    for (var i = 0; i < capabilitiesChars.size(); i++) {
      if (capabilitiesChars[i].toString().equals(",") || (i + 1) == capabilitiesChars.size()) {
        var stringWord = "";
        for (var j = 0; j < word.size(); j++) {
          stringWord += word[j];
        }
        // var stringWord = word.toString();
        mCapabilities.add(stringWord);
        word = [];
      } else {
        // if capability is supported
        if (
          !capabilitiesChars[i].toString().equals("[") &&
          !capabilitiesChars[i].toString().equals("]") &&
          !capabilitiesChars[i].toString().equals(" ")
        ) {
          word.add(capabilitiesChars[i]);
        }
      }
    }
  }
}
