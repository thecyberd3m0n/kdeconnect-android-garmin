package kde.kdeconnect;

import java.util.List;
import com.garmin.android.connectiq.IQDevice;
import com.garmin.android.connectiq.IQApp;

public interface BackgroundIQDeviceEvents {
    void onIQServiceUnavailable();
    void onIQApplicationNotInstalled();
    void onDisconnected();
    void onIQSDKReady();
    void onDeviceConnected(IQDevice iqDevice);
    void onAppConnected(IQApp iqApp);
}
