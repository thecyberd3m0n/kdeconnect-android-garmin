/*
 * SPDX-FileCopyrightText: 2014 Albert Vaca Cintora <albertvaka@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

package org.kde.kdeconnect.UserInterface.List;

import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.garmin.android.connectiq.IQDevice;

import org.kde.kdeconnect_garmin.R;
import org.kde.kdeconnect_garmin.databinding.ListItemWithIconEntryBinding;

public class GarminDeviceItem implements ListAdapter.Item {
    private final IQDevice iqDevice;

    public GarminDeviceItem(IQDevice device) {
        this.iqDevice = device;
    }

    public IQDevice getIqDevice() {
        return this.iqDevice;
    }

    @NonNull
    @Override
    public View inflateView(@NonNull LayoutInflater layoutInflater) {
        final ListItemWithIconEntryBinding binding = ListItemWithIconEntryBinding.inflate(layoutInflater);

//        binding.listItemEntryIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_device_phone_32dp));
        binding.listItemEntryTitle.setText(iqDevice.getFriendlyName());
// TODO:
//        if (device.compareProtocolVersion() != 0) {
//            if (device.compareProtocolVersion() > 0) {
//                binding.listItemEntrySummary.setText(R.string.protocol_version_newer);
//                binding.listItemEntrySummary.setVisibility(View.VISIBLE);
//            } else {
//                //FIXME: Uncoment when we decide old versions are old enough to notify the user.
//                binding.listItemEntrySummary.setVisibility(View.GONE);
//                /*
//                summaryView.setText(R.string.protocol_version_older);
//                summaryView.setVisibility(View.VISIBLE);
//                */
//            }
//        } else {
//            binding.listItemEntrySummary.setVisibility(View.GONE);
//        }

        return binding.getRoot();
    }

}
