package org.kde.kdeconnect;

import java.util.ArrayList;

public interface CIQMessageReceivedListener {
    void CIQMessageReceived(ArrayList<String> data);
}
