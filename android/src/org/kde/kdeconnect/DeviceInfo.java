package org.kde.kdeconnect;

public class DeviceInfo {
    String deviceId;
    String name;
    Boolean pairStatus;

    public DeviceInfo(String deviceId, String name, Boolean pairStatus) {
        this.deviceId = deviceId;
        this.name = name;
        this.pairStatus = pairStatus;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPairStatus() {
        return pairStatus;
    }

    public void setPairStatus(Boolean pairStatus) {
        this.pairStatus = pairStatus;
    }

    public String toString() {
        return (this.pairStatus ? "1" : "0") + ":" + this.name;
    }

    public static DeviceInfo fromDevice(Device device) {
        return new DeviceInfo(device.getDeviceId(),device.getName(),device.isPaired());
    }
}
