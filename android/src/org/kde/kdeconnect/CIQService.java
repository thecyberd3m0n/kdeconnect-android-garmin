package org.kde.kdeconnect;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.garmin.android.connectiq.ConnectIQ;
import com.garmin.android.connectiq.IQApp;
import com.garmin.android.connectiq.IQDevice;
import com.garmin.android.connectiq.exception.InvalidStateException;
import com.garmin.android.connectiq.exception.ServiceUnavailableException;

import org.kde.kdeconnect.Helpers.DeviceHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class CIQService extends Service implements ConnectIQ.IQDeviceEventListener, ConnectIQ.ConnectIQListener, ConnectIQ.IQApplicationInfoListener {
    public static final String iqAppID = "bed500dc-5f9a-46b6-be70-80fe560c2eb2";
    private static CIQService instance;

    private boolean iqSdkReady = false;
    private ConnectIQ iqConnect;
    public IQDevice iqDevice;
    private IQApp iqApp;

    private final ConcurrentHashMap<String, CIQMessageReceivedListener> messageReceivedListeners = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, ConcurrentHashMap<String, CIQMessageReceivedListener>> pluginMessageReceivedListeners = new ConcurrentHashMap<>();
    private Handler handler;
    private ArrayList<String> lastMessage = new ArrayList<>();
    private ArrayList<String> lastReceivedMessage = new ArrayList<>();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDeviceStatusChanged(IQDevice iqDevice, IQDevice.IQDeviceStatus iqDeviceStatus) {
        Log.d("CIQService", "IQ Device status changed");
        Log.d("CIQService", iqDeviceStatus.toString());
        if (iqDeviceStatus.equals(IQDevice.IQDeviceStatus.CONNECTED)) {
            this.syncStatusWithGarmin();
        } else {
            Log.d("CIQService", "IQ Device disconnected, should stop the service");
        }
    }

    @Override
    public void onSdkReady() {
        Log.d("KDE/CIQService", "-> onSdkReady");
        loadGarminDevices();
        iqSdkReady = true;
    }

    @Override
    public void onInitializeError(ConnectIQ.IQSdkErrorStatus iqSdkErrorStatus) {
        Log.e("KDE/CIQService", "-> onInitializeError");
        Log.e("KDE/CIQService", iqSdkErrorStatus.toString());
    }

    @Override
    public void onSdkShutDown() {
        Log.d("KDE/CIQService", "-> onSdkShutDown");
        iqSdkReady = false;
    }

    @Override
    public void onApplicationInfoReceived(IQApp loadedApp) {
        Log.d("KDE/CIQService", "Connect IQ application info received.");

        iqApp = loadedApp;
        try {
            this.listenIQMessages();
        } catch (InvalidStateException e) {
            e.printStackTrace();
        }
        this.syncStatusWithGarmin();
//            listenIQMessages();

    }

    @Override
    public void onApplicationNotInstalled(String s) {

    }

    public void loadGarminDevices() {
        List<IQDevice> paired = null;
        try {
            paired = iqConnect.getKnownDevices();
        } catch (InvalidStateException | ServiceUnavailableException e) {
            e.printStackTrace();
        }

        if (paired != null && paired.size() > 0) {

            for (IQDevice device : paired) {
                IQDevice.IQDeviceStatus status = null;
                try {
                    status = iqConnect.getDeviceStatus(device);
                } catch (InvalidStateException | ServiceUnavailableException e) {
                    e.printStackTrace();
                }
                if (status == IQDevice.IQDeviceStatus.CONNECTED) {
                    // Work with the device
                    iqDevice = device;
                    // set DevicesHelper's name
                    String deviceName = ((IQDevice) device).getFriendlyName();
                    Log.d("KDE/CIQService", String.format("Garmin device %s connected", deviceName));

                    //----------------------------------------------------------------
                    DeviceHelper.setDeviceName(this, deviceName);
                    // set device to garmin devices array

                    try {
                        this.loadIQAppInfo();
                    } catch (InvalidStateException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void loadIQAppInfo() throws InvalidStateException {
        try {
            iqConnect.getApplicationInfo(CIQService.iqAppID, iqDevice, this);
        } catch (ServiceUnavailableException e) {
            e.printStackTrace();
        }

    }

    private void listenIQMessages() throws InvalidStateException {
        iqConnect.registerForAppEvents(iqDevice, iqApp, (device, app, messageData, status) -> {
            if (status == ConnectIQ.IQMessageStatus.SUCCESS) {
                if (!(((ArrayList) messageData.get(0)).get(0).equals("plugin"))) {
                    this.messageReceivedListeners.forEach(new BiConsumer<String, CIQMessageReceivedListener>() {
                        @Override
                        public void accept(String s, CIQMessageReceivedListener listener) {
                            ArrayList<String> message = (ArrayList<String>) messageData.get(0);
                            if (s.equals(message.get(0))) {
                                if (lastReceivedMessage.hashCode() == message.hashCode()) {
                                    Log.d("CIQService", "Received duplicate message and dropped it");
                                    return;
                                }
                                lastReceivedMessage = message;

                                listener.CIQMessageReceived(message);
                            }
                        }
                    });

                } else {
                    String pluginName = ((ArrayList<String>) messageData.get(0)).get(1);
                    ConcurrentHashMap<String, CIQMessageReceivedListener> listener = this.pluginMessageReceivedListeners.get(pluginName);
                    if (listener == null) {
                        Log.d("CIQService", "Received plugin message but not found related plugin");
                        return;
                    }
                    this.pluginMessageReceivedListeners.get(pluginName).forEach(new BiConsumer<String, CIQMessageReceivedListener>() {
                        @Override
                        public void accept(String s, CIQMessageReceivedListener listener) {
                            ArrayList<String> message = (ArrayList<String>) messageData.get(0);
                            if (message.get(2).equals(s)) {
                                if (lastReceivedMessage.hashCode() == message.hashCode()) {
                                    Log.d("CIQService", "Received duplicate plugin message and dropped it");
                                    return;
                                }
                                lastReceivedMessage = message;
                                listener.CIQMessageReceived(message);
                            }
                        }
                    });
                }

                Log.d("KDE/CIQService", "Incoming message from ConnectIQ");

            }
        });
    }

    public void addPluginCIQMessageReceiver(String plugin, String command, CIQMessageReceivedListener listener) {
        if (this.pluginMessageReceivedListeners.get(plugin) == null) {
            ConcurrentHashMap<String, CIQMessageReceivedListener> hashMap = new ConcurrentHashMap<>();
            this.pluginMessageReceivedListeners.put(plugin, hashMap);
        }
        Objects.requireNonNull(this.pluginMessageReceivedListeners.get(plugin)).put(command, listener);
    }

    private void addCIQMessageReceiver(String command, CIQMessageReceivedListener listener) {
        this.messageReceivedListeners.put(command, listener);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        handler = new android.os.Handler();
        handler.postDelayed(
                new Runnable() {
                    public void run() {
                        loadConnectIQ();
                        BackgroundService.getInstance().addDeviceListChangedCallback("CIQService", () -> {
                            syncStatusWithGarmin();
                        });
                    }
                }, 7000);
    }

    public static CIQService getInstance() {
        return instance;
    }

    private void syncStatusWithGarmin() {
        // collect devices from below to "sync" method
        ConcurrentHashMap<String, DeviceInfo> deviceInfoMap = new ConcurrentHashMap<>();
        ArrayList<String> message = new ArrayList<>();

        BackgroundService.getInstance().getDevices().forEach(new BiConsumer<String, Device>() {
            @Override
            public void accept(String s, Device device) {
                message.add(device.getDeviceId());
                message.add(device.getName());
                message.add(String.valueOf(device.getDeviceType()));
                message.add(String.valueOf(device.isPaired()));

                ArrayList<String> deviceSupportedPlugins = new ArrayList<>(device.getSupportedPlugins());
                String[] pluginsSupportedByWatch = new String[]{"RunCommandPlugin"};
                ArrayList<String> commonPlugins = new ArrayList<String>();
                Arrays.stream(pluginsSupportedByWatch).forEach(new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        if (deviceSupportedPlugins.contains(s)) {
                            commonPlugins.add(s);
                        }
                    }
                });
                message.add(String.valueOf(commonPlugins));
            }
        });

        this.sendCIQMessage("sync", message);
        this.addCIQMessageReceiver("sync", new CIQMessageReceivedListener() {
            @Override
            public void CIQMessageReceived(ArrayList<String> data) {
                Log.d("CIQService", "Sync message received from Garmin");
                Log.d("CIQService", data.toString());
            }
        });

        this.addCIQMessageReceiver("requestpairing", new CIQMessageReceivedListener() {
            @Override
            public void CIQMessageReceived(ArrayList<String> data) {
                Log.d("CIQService", "request pairing for data");
                String deviceId = data.get(1);
                Device device = BackgroundService.getInstance().getDeviceById(deviceId);
                if (device == null) {
//                    TODO: return message to watch
                    Log.e("CIQService", "Device not found");
                }
                device.addPairingCallback(new Device.PairingCallback() {
                    @Override
                    public void incomingRequest() {
                        Log.d("CIQService", "Pairing requested");
                    }

                    @Override
                    public void pairingSuccessful() {
                        Log.d("CIQService", "Pairing successfull");
                        ArrayList<String> message = new ArrayList<>();
                        message.add(deviceId);
                        message.add("allow");
//                        don't send it same moment as "sync"

                        handler.postDelayed(
                                new Runnable() {
                                    public void run() {
                                        CIQService.this.sendCIQMessage("requestpairing", message);
                                    }
                                }, 500);
                    }

                    @Override
                    public void pairingFailed(String error) {
                        Log.e("CIQService", "pairing failed");
                        Log.e("CIQService", error);
                        ArrayList<String> message = new ArrayList<>();
                        message.add(deviceId);
                        message.add("reject");
                        CIQService.this.sendCIQMessage("requestpairing", message);
                    }

                    @Override
                    public void unpaired() {
                        ArrayList<String> message = new ArrayList<String>();
                        message.add(deviceId);
                        CIQService.this.sendCIQMessage("unpaired", message);
                    }
                });
                device.requestPairing();
            }
        });
    }

    public void sendCIQPluginMessage(String plugin, String command, ArrayList<String> message) {
        message.add(0, "plugin");
        message.add(1, plugin);
        message.add(2, command);
        if (this.lastMessage.hashCode() == message.hashCode()) {
            Log.d("CIQService", "Message dropped - tried to send same plugin message while previous not ended");
            return;
        }
        Log.d("CIQService", "Sending plugin message to Garmin");

        this.lastMessage = message;

        try {
            iqConnect.sendMessage(iqDevice, iqApp, message, new ConnectIQ.IQSendMessageListener() {
                @Override
                public void onMessageStatus(IQDevice iqDevice, IQApp iqApp, ConnectIQ.IQMessageStatus iqMessageStatus) {
                    Log.d("CIQService", "Plugin message sent to Garmin");
                    lastMessage = new ArrayList<>();
                    if (iqMessageStatus != ConnectIQ.IQMessageStatus.SUCCESS) {
                        Log.e("CIQService", "Failed to respond to message 'kdedevices', message status is");
                        Log.e("CIQService", iqMessageStatus.toString());
                    }
                }
            });
        } catch (InvalidStateException exception) {
            Log.e("CIQService", "Garmin Connect invalid state");
            Log.e("CIQService", exception.getMessage());
        } catch (ServiceUnavailableException exception) {
            Log.e("CIQService", "Garmin Connect Service Unavailable");
            Log.e("CIQService", exception.getMessage());
        }


    }

    private void sendCIQMessage(String command, ArrayList<String> message) {
        message.add(0, command);

        if (this.lastMessage.hashCode() == message.hashCode()) {
            Log.d("CIQService", "Message dropped - tried to send same message while previous not ended");
            return;
        }
        this.lastMessage = message;
        Log.d("CIQService", "Sending message to Garmin");
        try {
            iqConnect.sendMessage(iqDevice, iqApp, message, new ConnectIQ.IQSendMessageListener() {
                @Override
                public void onMessageStatus(IQDevice iqDevice, IQApp iqApp, ConnectIQ.IQMessageStatus iqMessageStatus) {
                    Log.d("CIQService", "Message sent to Garmin");
                    lastMessage = new ArrayList<>();
                    if (iqMessageStatus != ConnectIQ.IQMessageStatus.SUCCESS) {
                        Log.e("CIQService", "Failed to respond to message 'kdedevices', message status is");
                        Log.e("CIQService", iqMessageStatus.toString());
                    }
                }
            });
        } catch (InvalidStateException exception) {
            Log.e("CIQService", "Garmin Connect invalid state");
            Log.e("CIQService", exception.getMessage());
        } catch (ServiceUnavailableException exception) {
            Log.e("CIQService", "Garmin Connect Service Unavailable");
            Log.e("CIQService", exception.getMessage());
        }

    }

    // List of known devices
    public void loadConnectIQ() {
        Log.d("GarminIQService", "->loadDevices");

        iqConnect = ConnectIQ.getInstance(this, ConnectIQ.IQConnectType.WIRELESS);

        // Initialize the SDK
        iqConnect.initialize(this, false, this);
    }
}
